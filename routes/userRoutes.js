const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");

const jwt = require('jsonwebtoken');


// Route for checking if the user's email already exists in the database
// Invokes the checkEmailExists function from the controller file to communicate with our database
// Passes the "body" property of our "request" object to the corresponding controller function
// The full route to access this is "http://localhost:4000/users/checkEmail" where the "/users" was defined in our "index.js" file
// The "then" method uses the result from the controller function and sends it back to the frontend application via the "res.send" method
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	//const isAdminData = auth.decode(req.headers.authorization).isAdmin
	//console.log(isAdminData)
	//console.log(userData);
	/*
	{
	  id: '62c28b720f41ec01d8422f78',
	  email: 'cplilagan@mail.com',
	  isAdmin: false,
	  iat: 1656984881
	}
	*/

	//console.log(req.headers.authorization)


	userController.detailsUser({id : userData.id}).then(resultFromController => res.send(resultFromController));
})

/*
router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))

});
*/

router.post("/enroll", auth.verify, (req, res) => {

	// 
	const userData = auth.decode(req.headers.authorization);
	
	console.log('userData')
	// console.log(userData)
	console.log(userData.id)
	
	

	let data = {
		userId: userData.id,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))

});



// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;

