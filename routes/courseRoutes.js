const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController');
const auth = require("../auth");


// Route for creating a course
/*router.post("/", (req, res) => {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
});*/


//Session 39 - Activity - Solution 1
// Route for creating a course for admin only
router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    console.log('courselog-- ' + userData.isAdmin);
    console.log(req.headers.authorization);

    if (userData.isAdmin) {

        courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));

    } else {

        res.send({ auth: "failed" });

    }


});

//Session 39 - Activity Solution 2
// router.post("/", auth.verify, (req, res) => {

//     const userData = auth.decode(req.headers.authorization)

//     courseController.addCourse(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
// })

// Route for retrieving all the courses
router.get("/all", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    courseController.getAllCourses(userData).then(resultFromController => res.send(resultFromController))
})

// Route for all active users
router.get("/", (req, res) => {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
});

// Route for retrieving a specific course
// localhost:4000/courses/542131654554
// courseId = 542131654554
// "/:parameterName"
router.get("/:courseId", (req, res) => {

	console.log(req.params)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
} )


// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))

})


// Route for updating a course by Admin
// S-40 Activity code
router.put("/:courseId/archive", auth.verify, (req, res) => {

            const userData = auth.decode(req.headers.authorization);

            if (userData.isAdmin) {

                courseController.deactivateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))

            } else {

                res.send({ auth: "failed" });
    }

})

module.exports = router;