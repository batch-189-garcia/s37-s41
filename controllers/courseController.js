const Course = require('../models/Course');
const User = require('../models/User');


// Create a new course
module.exports.addCourse = (reqBody) => {

    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    });


    return newCourse.save().then((course, error) => {

        if (error) {

            return false

        } else {

            return true
        }
    })
}

//Activity Solution 2
// module.exports.addCourse = (reqBody, userData) => {

//     return User.findById(userData.userId).then(result => {

//         if (userData.isAdmin == false) {
//             return "You are not an admin"
//         } else {
//             let newCourse = new Course({
//                 name: reqBody.name,
//                 description: reqBody.description,
//                 price: reqBody.price
//             })

//             //Saves the created object to the database
//             return newCourse.save().then((course, error) => {
//                 //Course creation failed
//                 if(error) {
//                     return false
//                 } else {
//                     //course creation successful
//                     return "Course creation successful"
//                 }
//             })
//         }

//     });    
// }



// Retrieve all courses
module.exports.getAllCourses = async (data) => {

    if (data.isAdmin) {

        return Course.find({}).then(result => {

            return result

        })
    } else {
    	
        return false
    }
}


module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {

		return result
	} )
}


module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {

		return result
	})
}


module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}


	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

		if(error) {
			return false

		} else {
			return true
		}
	})

}


// de-activate course
module.exports.deactivateCourse = (reqParams, reqBody) => {

	let deactivateCourse = {

		isActive: reqBody.isActive
	}


	return Course.findByIdAndUpdate(reqParams.courseId, deactivateCourse).then((course, error) => {

		if(error) {
			return false

		} else {
			return true
		}
	})

}
