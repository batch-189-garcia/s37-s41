const jwt = require('jsonwebtoken');
// User defined string data that will be used to create our JSON web tokens
// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword
const secret = "CourseBookingAPIbgarcia"; //user defined

/*
- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
- Information is kept secure through the use of the secret code
- Only the system that knows the secret code that can decode the encrypted information
*/



module.exports.createAccessToken = (user) => {

	// The data will be received from the registration form
	// When the user logs in, a token will be created with user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	// Generate a JSON web token using the jwt's sign method
	// Generates the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {}) //(<payload>, secretkey, {})

}


module.exports.verify = (req, res, next) => {

	//console.log(req.headers)
	// console.log('auth.js '+req.headers)

	let token = req.headers.authorization;

	if (typeof token !== "undefined") {

		console.log(token);
		//Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyYzI4YjcyMGY0MWVjMDFkODQyMmY3OCIsImVtYWlsIjoiY3BsaWxhZ2FuQG1haWwuY29tIiwiaXNBZG1pbiI6ZmFsc2UsImlhdCI6MTY1Njk4NDg4MX0.6laFrOkNM-bpI0tO-fAh5rJY332eWWvY8asrubQt-v8


		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {

			if(err) {
				return res.send({auth: "failed"})
			}	else {

				next ()
			}
		})

	} else {

		return res.send({auth: "failed"});
	}
}


module.exports.decode = (token) => {

	//Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyYzI4YjcyMGY0MWVjMDFkODQyMmY3OCIsImVtYWlsIjoiY3BsaWxhZ2FuQG1haWwuY29tIiwiaXNBZG1pbiI6ZmFsc2UsImlhdCI6MTY1Njk4NDg4MX0.6laFrOkNM-bpI0tO-fAh5rJY332eWWvY8asrubQt-v8
	console.log(token)


	if(typeof token !== "undefined") {

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if (err) {

				return null
			} else {

				return jwt.decode(token, {complete: true}).payload //payload is id,email,isAdmin,iat
				console.log('payload '+payload)
			}
		})
	} else {
		return null
	}
}